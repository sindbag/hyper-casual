using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class S_MenuController : MonoBehaviour
{
    public string GameLevel;
    public Text Score, HighScore;
    S_ScoreManager scoreManager;

    private void Start()
    {
        scoreManager = FindObjectOfType<S_ScoreManager>();
        Score.text = scoreManager.CurrentScore.ToString();
        HighScore.text = scoreManager.HighScore.ToString();
    }

    public void StartGame()
    {
        SceneManager.LoadScene(GameLevel);
    }
}
