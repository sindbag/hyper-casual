using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_ObstacleSet : MonoBehaviour
{
    public float speed = 5.0f;
    public float speedStep = 0.5f;
    public GameObject obstacle1;
    public GameObject obstacle2;
    public GameObject bonus;
    public List<int> order = new List<int> {0, 1, 2};
    public S_Manager manager;

    List<int> RandomOrder(List<int> alpha)
    {
        List<int> beta = alpha;
        for (int i = 0; i < beta.Count; i++)
        {
            int temp = beta[i];
            int randomIndex = Random.Range(i, beta.Count);
            beta[i] = beta[randomIndex];
            beta[randomIndex] = temp;
        }
        return beta;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 NewPos = transform.position;
        NewPos.x -= speed* Time.deltaTime;
        transform.position = NewPos;
        if (transform.position.x <= -15.0f) GenerateSet();
    }

    void GenerateSet()
    {
        gameObject.transform.position = new Vector2(15, 0);
        order = RandomOrder(order);
        obstacle1.transform.position = new Vector3(order[0] * 5, -3.5f, 0) + gameObject.transform.position;
        obstacle2.transform.position = new Vector3(order[1] * 5, -2.5f, 0) + gameObject.transform.position;

        bonus.GetComponent<S_Bonus>().Activate();
        bonus.transform.position = new Vector3(order[2]*5, -4.0f, 0) + gameObject.transform.position;
        foreach (var obj in FindObjectsOfType<S_ObstacleSet>())
        {
            obj.IncreaseSpeed();
        }
    }

    void IncreaseSpeed()
    {
        speed += speedStep;
    }
}
