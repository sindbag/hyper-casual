using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_Bonus : MonoBehaviour
{
    public int Score = 1;
    S_Manager manager;
    Rigidbody2D rb;
    CircleCollider2D circle;
    SpriteRenderer sprite;
    void Start()
    {
        manager = FindObjectOfType<S_Manager>();
        rb = GetComponent<Rigidbody2D>();
        circle = GetComponent<CircleCollider2D>();
        sprite = GetComponent<SpriteRenderer>();
    }

    public void Deactivate()
    {
        circle.enabled = false;
        sprite.enabled = false;
        rb.simulated = false;
        rb.velocity = Vector2.zero;
    }

    public void Activate()
    {
        circle.enabled = true;
        sprite.enabled = true;
        rb.simulated = true;
    }

    public void BonusCollected()
    {
        manager.IncreaseScore(Score);
        Deactivate();
        
    }
}
