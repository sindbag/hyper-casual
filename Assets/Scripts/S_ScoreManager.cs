using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_ScoreManager : MonoBehaviour
{
    public int CurrentScore, HighScore = 0;

    private void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("ScoreManager");

        if (objs.Length > 1)
        {
            foreach(var go in objs)
            {
                S_ScoreManager sc = go.GetComponent<S_ScoreManager>();
                if (HighScore >= sc.HighScore) Destroy(go);
            }
        }

        DontDestroyOnLoad(this.gameObject);
    }
}
