using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_PlayerController : MonoBehaviour
{
    public Rigidbody2D rigidBody;
    public float impulsePower;
    public S_Manager manager;
    public bool canJump = true;

    void Update()
    {
        if (((Input.GetKeyDown(KeyCode.Space)) || (Input.GetKeyDown(KeyCode.Mouse0))) && canJump)
        {
            rigidBody.velocity = Vector2.zero;
            rigidBody.AddForce(Vector2.up * impulsePower);
            canJump = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        S_Bonus bonus = collision.gameObject.GetComponent<S_Bonus>();
        if (bonus != null)
        {
            Debug.Log("Bonus");
            bonus.BonusCollected();
        }
        else
        {
            S_ObstacleSet obstacle = collision.gameObject.GetComponentInParent<S_ObstacleSet>();
            if (obstacle != null) manager.GameOver();
            else canJump = true;
        }

    }
}
