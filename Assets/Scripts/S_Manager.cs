using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class S_Manager : MonoBehaviour
{
    public int CurrentScore, HighScore;
    public Button PauseButton;
    public Canvas PauseMenu;
    public Text Score;
    public S_PlayerController PlayerController;
    public S_ScoreManager ScoreManager;

    private void Start()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("ScoreManager");

        ScoreManager = objs[0].GetComponent<S_ScoreManager>();
        ScoreManager.CurrentScore = 0;
    }

    public void GameOver()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("GameLevel");
    }

    public void ExitToMainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }

    public void Restart()
    {
        GameOver();
    }

    public void Pause()
    {
        Time.timeScale = 0;
        PauseMenu.enabled = true;
        PauseButton.enabled = false;
        PlayerController.canJump = false;
    }

    public void Resume()
    {
        Time.timeScale = 1;
        PauseMenu.enabled = false;
        PauseButton.enabled = true;
        PlayerController.canJump = true;
    }

    void StoreScores()
    {
        UpdateHighScore();
        if (HighScore > ScoreManager.HighScore) ScoreManager.HighScore = HighScore;
        ScoreManager.CurrentScore = CurrentScore;
    }

    void UpdateHighScore()
    {
        if (HighScore < CurrentScore) HighScore = CurrentScore;
    }

    public void IncreaseScore(int Delta)
    {
        CurrentScore += Delta;
        Score.text = CurrentScore.ToString();
        StoreScores();
    }
}
